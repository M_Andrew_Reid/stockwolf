package mandrewreid.stockwolf;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class WolfConsole {
	
	private PrintWriter out;
	private Scanner in;

	public WolfConsole(InputStream input, OutputStream output) {
		this.out = new PrintWriter(output, true);
		this.in = new Scanner(input);
	}

}
