package mandrewreid.stockwolf;

import java.util.Scanner;

import org.json.JSONObject;

public class WolfMain {
	
	protected StockService stockService;
	protected WolfConsole console;

	public static void main(String[] args) {

		WolfMain wolfMain = new WolfMain(new WolfConsole(System.in, System.out), new StockService());
		
		wolfMain.run();
	} // end main

	public WolfMain(WolfConsole console, StockService stockService) {
		this.console = console;
		this.stockService = stockService;

	}

	public void run() {

		searchStock();
	}

	public  void searchStock() {
		String key = "RL2T37F5J0TOG2C2";
		String ticker = "";
		boolean entryVerified = false;

		Scanner myScanner = new Scanner(System.in);
		String quoteString = "";

		while (entryVerified == false) {
			System.out.println("Please enter a stock Symbol:");
			ticker = myScanner.nextLine();

			if (ticker.length() < 1 || ticker == null) {
				System.out.println("Invalid ticker. Please try again.");
				continue;
			}

			quoteString = stockService.quote(key, ticker).toString();

			if (quoteString.length() < 75) {

				System.out.println("Invalid ticker. Please try again.");

			} else {

				entryVerified = true;

			}

		}
		
		
		/*{
	    "Global Quote": {
	        "01. symbol": "IBM",
	        "02. open": "117.6000",
	        "03. high": "118.0400",
	        "04. low": "116.6900",
	        "05. price": "116.9400",
	        "06. volume": "5024593",
	        "07. latest trading day": "2020-11-20",
	        "08. previous close": "117.1800",
	        "09. change": "-0.2400",
	        "10. change percent": "-0.2048%"
	    }
	}*/
		
		
		
		JSONObject quoteJson = new JSONObject(quoteString);

		String stockTicker = quoteJson.getJSONObject("Global Quote").getString("01. symbol");
		String date = quoteJson.getJSONObject("Global Quote").getString("07. latest trading day");
		String closingPrice = quoteJson.getJSONObject("Global Quote").getString("05. price");
		String previousClose = quoteJson.getJSONObject("Global Quote").getString("08. previous close");
		String volume = quoteJson.getJSONObject("Global Quote").getString("06. volume");
		String dayChange = quoteJson.getJSONObject("Global Quote").getString("10. change percent");

		System.out.println();
		System.out.println();
		System.out.println("Information for: " + stockTicker + ", latest trading day: " + date);
		System.out.println("Price: " + closingPrice);
		System.out.println("Previous Day's Close: " + previousClose);
		System.out.println("Daily Volume: " + volume);
		System.out.println("Change in Price for Day: " + dayChange);

		System.out.println();
		System.out.println();
		System.out.println();

		searchStock();

	}

}// class
