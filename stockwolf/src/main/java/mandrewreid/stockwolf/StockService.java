package mandrewreid.stockwolf;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class StockService {

	RestTemplate restTemplate;
	
	public StockService() {
		this.restTemplate = new RestTemplate();
	}


	
	
	
	public Object test(String key, String ticker) {
	ResponseEntity response = restTemplate.getForEntity(
		                "https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=" + ticker + "&apikey=" + key, 
		                String.class); 

		return response.getBody();

	}
	
}
